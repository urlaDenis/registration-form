import {useState} from 'react';

const WindowWidth = () => {
	const [windowWidth, setWindowWidth] = useState(null);

	window.onresize = () => {
		setWindowWidth(window.innerWidth);
	};

	return windowWidth;
};

export default WindowWidth;