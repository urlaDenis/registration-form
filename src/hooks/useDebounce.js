import { useEffect } from 'react';

const UseDebounce = (value, time) => {

	useEffect(() => {
		const timer = setTimeout(value, time);

		return () => clearTimeout(timer)
	});

	return value;
};

export default UseDebounce;