import { useState, useEffect } from 'react';
import useDebounce from './useDebounce';
import useWindowWidth from './useWindowWidth';

const deviceType = (width) => {
	if (width < 413) {
		return 'mob-sm';
	} else if (width < 767) {
		return 'mob';
	} else if (width < 1023) {
		return 'mob-lg';
	}

	return 'desk';
};

const CurrentDevice = () => {
	const [device, setDevice] = useState(null);
	const width = useDebounce(useWindowWidth, 300)

	useEffect(() => {
		setDevice(deviceType(width))
	});

	return device;
};

export default CurrentDevice;