import { fromJS } from 'immutable';


// constants
export const NEXT_STEP = 'NEXT_STEP';


export const SWITCH = 'SWITCH';


// initial
export const initialState = fromJS({
	device: '',
	stepData: {
		steps: [
			'Main Data',
			'Additional Data',
			'Confirmation',
		],
		currentStep: 0,
	},
	userData: {
		userName: '',
		userEmail: '',
		userPhone: '',
		userAdditionalPhones: [],
	},
});


// utils
export const getActionName = (name, action) => `${name}_${action}`;

// action creators
export const nextStepSwitch = (payload) => (
	{ type: getActionName(NEXT_STEP, SWITCH), payload }
);

// reducer
const reducer = (state, { type, payload }) => {

	console.log(`${type} then ==>`);

	switch (type) {
		case getActionName(NEXT_STEP, SWITCH):
			return state
				.setIn(['stepData', 'currentStep'], payload.nextStep);

		default:
			return state;
	}
};

export default reducer;