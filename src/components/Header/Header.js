import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
	header: {
		padding: '20px',
	},
	title: {
		fontWeight: 600,
	}
});


const Header = () => {
	const classes = useStyles();

	return (
		<div className={classes.header}>
			<Container maxWidth="md">
				<Grid container justify="center">
					<Typography variant="h5" className={classes.title}>
						User Registration Form
					</Typography>
				</Grid>
			</Container>
		</div>
	);
};

export default Header;