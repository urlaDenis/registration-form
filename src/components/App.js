//import React, { useEffect } from 'react';
import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/styles';
import From from './Form/Form';
import { useStateValue } from './StoreProvider';
//import { deviceTypeSet } from '../ducks/form';
import 'typeface-roboto';
import desktopImage from '../images/background.jpg';
import mobileImage from '../images/background-mobile.jpg';
import currentDevice from '../hooks/currentDevice';
import useDebounce from '../hooks/useDebounce';

const useStyles = makeStyles({
	appBody: {
		backgroundPosition: 'center',
		backgroundRepeat: 'no-repeat',
		backgroundSize: 'contain',
		minHeight: '100%',
		padding: '10%',
	},
});

const App = () => {
	const device = currentDevice();
	console.log(device);
	//useDebounce(() => console.log('debounced'), 300)

	const classes = useStyles();
    const image = mobileImage;
    //const image = device === 'mob' || device === 'mob-sm'
	//	? mobileImage
	//	: desktopImage;

    return (
    	<div className={classes.appBody} style={{ background: `url(${image})` }}>
			<CssBaseline />
			<From />
		</div>
    );
};

export default App;
