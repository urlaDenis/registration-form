import React, {useEffect, useState} from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
	helper: {
		transition: 'opacity 200ms',
		opacity: '0',
	},
	displayHelper: {
		opacity: '1',
	},
});

const TextFieldComponent = (props) => {
	const {
		type,
		placeholder,
		error,
		required,
		fullWidth,
		label,
		helperText,
		value,
		onChange,
	} = props;
	const classes = useStyles();

	const [helpText, setHelText] = useState(helperText);
	const [helpMessageIsShow, setHelpMessageIsShow] = useState(false);
	const [errorMessage, setErrorMessage] = useState(null);

	useEffect(() => {
		setErrorMessage(error);
	}, [error]);

	if (errorMessage && !helpMessageIsShow) {
		setHelpMessageIsShow(true);
		setHelText(error);
	}

	const onClickHandler = () => {
		setHelpMessageIsShow(false);
		setErrorMessage(null);

		setTimeout(() => {
			setHelText(helperText);
		}, 200);
	};

	const onChangeHandler = (e) => {
		setHelpMessageIsShow(true);
		onChange(e.target.value);
	};

	return (
		<TextField
			className={useStyles().main}
			placeholder={placeholder}
			id={label}
			error={errorMessage}
			required={required}
			fullWidth={fullWidth}
			label={label}
			value={value}
			type={type}
			helperText={helpText}
			onClick={onClickHandler}
			onChange={onChangeHandler}
			onBlur={() => setHelpMessageIsShow(false)}
			FormHelperTextProps={{ className: `
					${classes.helper}
					${helpMessageIsShow
						? classes.displayHelper
						: null}
				`}}
		/>

	);
};

export default TextFieldComponent;