import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
	title: {
		marginBottom: '5px',
	},
});

const StepTitle = ({ title }) => {
	const classes = useStyles();

	return (
		<Typography variant="h6" className={classes.title}>
			{title}
		</Typography>
	);
};

export default StepTitle;