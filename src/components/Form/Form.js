import React from 'react';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/styles';
import Header from '../Header/Header';
import Steps from './Steps';
import StepsBodies from './StepsBodies';
import StepsControls from './StepsControls';
import { useStateValue } from '../StoreProvider';

const useStyles = makeStyles({
	container: {
		borderRadius: '5px',
		overflow: 'hidden',
		padding: '0',
	},
});

const Form = () => {
	const [state, dispatch] = useStateValue(); // eslint-disable-line

	const classes = useStyles();
	const steps = state.getIn(['stepData', 'steps']);
	const currentStep = state.getIn(['stepData', 'currentStep']);
	const device = state.get('device');

	return (
		<Container maxWidth="sm" className={classes.container}>
			<Paper>
				<Header />
				<Steps steps={steps} currentStep={currentStep} device={device} />
				{
					device === 'desk' || device === 'mob-lg'
						? (
							<>
								<StepsBodies currentStep={currentStep} />
								<StepsControls currentStep={currentStep} />
							</>
						) : null
				}
			</Paper>
		</Container>
	);
};

export default Form;