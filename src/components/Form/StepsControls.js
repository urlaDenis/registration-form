import React from 'react';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/styles';
import { useStateValue } from '../StoreProvider';
import { nextStepSwitch } from '../../ducks/form';

const useStyles = makeStyles({
	buttonsHolder: {
		display: 'flex',
		justifyContent: 'flex-end',
		padding: '20px'
	},
});

const StepsControls = () => {
	const [state, dispatch] = useStateValue(); // eslint-disable-line

	const currentStep = state.getIn(['stepData', 'currentStep']);

	const onNextStepClickHandler = () => {
		if (currentStep !== 2) {
			const nextStep = currentStep + 1;

			dispatch(nextStepSwitch({ nextStep }));
		}
	};

	const onBackClickHandler = () => {
		if (currentStep !== 0) {
			const nextStep = currentStep - 1;

			dispatch(nextStepSwitch({ nextStep }));
		}
	};

	return (
		<Container className={useStyles().buttonsHolder}>
			<Button
				variant="contained"
				onClick={onBackClickHandler}
			>
				Back
			</Button>
			<Button
				variant="contained"
				color="primary"
				onClick={onNextStepClickHandler}
			>
				Next
			</Button>
		</Container>
	)
};

export default StepsControls;