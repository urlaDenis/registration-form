import React from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import StepsBodies from './StepsBodies';
import StepsControls from './StepsControls';

const Steps = ({ steps, currentStep, device }) => {

	return (
		<Stepper
			activeStep={currentStep}
			orientation={
				device === 'mob' || device === 'mob-sm'
					? 'vertical'
					: 'horizontal'
			}
		>
			{steps.map((label, id) => (
				<Step key={label}>
					<StepLabel>
						{label}
					</StepLabel>
					{
						device === 'mob' || device === 'mob-sm'
							? (
								<StepContent>
									<StepsBodies currentStep={id} />
									<StepsControls currentStep={id} />
								</StepContent>
							) : null
					}
				</Step>
			))}
		</Stepper>
	)
};

export default Steps;