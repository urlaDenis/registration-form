import React from 'react';
import Container from '@material-ui/core/Container';
import MainUserData from './Steps/MainUserData';
import AdditionalUserData from './Steps/AdditionalUserData';
import Confirmation from './Steps/Confirmation';

const StepsControls = ({ currentStep }) => {

	const getStep = (stepId) => {
		switch (stepId) {
			case 0:
				return <MainUserData />;
			case 1:
				return <AdditionalUserData />;
			case 2:
				return <Confirmation />;
			default:
				throw new Error('Unknown step');
		}
	};

	return (
		<Container>
			{getStep(currentStep)}
		</Container>
	)
};

export default StepsControls;