import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextFieldComponent from '../../Common/TextFieldComponent';
import { useStateValue } from '../../StoreProvider';

const userNameHelpMessage = 'Name will be login in future.';
const userEmailHelpMessage = 'Enter your valid email.';
const userPasswordHelpMessage = 'Passwords must be at least 6 characters long.';
const userSecondPasswordHelpMessage = 'Must match previous entry.';

const MainUserData = () => {
	const [state, dispatch] = useStateValue(); // eslint-disable-line
	const [userName, setUserName] = useState('');
	const [userEmail, setUserEmail] = useState('');
	const [userPassword, setUserPassword] = useState('');

	const device = state.get('device');

	return(
		<>
			<Grid
				container
				justify="center"
				spacing={1}
			>
				<Grid item xs={12} sm={8}>
					<TextFieldComponent
						required
						fullWidth
						value={userName}
						onChange={setUserName}
						label="User Name"
						helperText={userNameHelpMessage}
					/>
				</Grid>
				<Grid item xs={12} sm={8}>
					<TextFieldComponent
						required
						fullWidth
						value={userEmail}
						onChange={setUserEmail}
						type="email"
						label="User Email"
						placeholder="exzample@host.com"
						helperText={userEmailHelpMessage}
					/>
				</Grid>
				<Grid item xs={12} sm={8}>
					<TextFieldComponent
						required
						fullWidth
						value={userPassword}
						onChange={setUserPassword}
						type="password"
						placeholder="Create password..."
						label="Password"
						helperText={userPasswordHelpMessage}
					/>
				</Grid>
				<Grid item xs={12} sm={8}>
					<TextFieldComponent
						placeholder="Repeat password..."
						required
						fullWidth
						label={`Confirm${device !== 'mob-sm' ? ' Password' : ' Pass...'}`}
						type="password"
						onChange={() => {}}
						helperText={userSecondPasswordHelpMessage}
					/>
				</Grid>
			</Grid>
		</>
	);
};

export default MainUserData;