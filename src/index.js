import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { StateProvider } from './components/StoreProvider';
import reducer, { initialState } from './ducks/form';
import './index.css';

ReactDOM.render(
	<StateProvider initialState={initialState} reducer={reducer}>
		<App />
	</StateProvider>,
	document.getElementById('root')
);
